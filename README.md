O
=

O is a cross-platform, big-data, cloud discovery engine that can be easily 
deployed and utilized by any-size company or entity to synergize and maximize 
its social crowdsourcing and global localization efforts. Remote sensing and 
auto-discovery of the motivated, effective, and creative social-niches which 
are local but act at global scale is of its essence. Combining new innovations 
and advancements in the fields of Machine Learning, Natural Language 
Processing, Information Retrieval, Reasoning, and more, with the mash-up of 
existing hi-techs, O has become a rapidly growing and an ever evolving platform. 
As a robust, dynamic, and extendable platform, O enables and empowers its grid 
members to do 
* large scale problem solving, 
* cross-platform strategic planning, 
* diverse functional analysis, 
* social discovery and engagement,
* convergence,
* and much much more. 

O will be at the core of the many innovations and emerging technologies. 
The Next Next Big Thing is coming.
